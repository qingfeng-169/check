<?php


namespace check;


class DateTime
{
    /**
     * @param string $time 时间字符串
     * @param string $format 时间格式
     * @return array
     */
    public static function checkTime($time, $format = 'Y-m-d H:i:s')
    {
        $formatTime = \DateTime::createFromFormat($format, $time)->format($format);
        if ($time == $formatTime) {
            return ['result' => true, 'message' => '时间格式正确'];
        } else {
            return ['result' => false, 'message' => '时间格式错误'];
        }
    }
}